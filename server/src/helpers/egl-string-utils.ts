import { Connection, Diagnostic, TextDocument } from 'vscode-languageserver';
import { LintSettings } from '../interfaces/lintingError';
import { EglStaticSectionParser } from './egl-static-section-parser';
import { EglAst } from './egl.ast';
import { Tokenizer } from './tokenizer';


export class EglSections {
	private _errors: Array<Diagnostic> = [];

	process(text: string, textDocument: TextDocument,
		connection: Connection, settings: LintSettings): Array<Diagnostic> {

		const lines = text.split("\n");
		const eglAst = new EglAst(settings);


		let lineNumber = 0;

		lines.forEach(line => {
			// Reject empty lines
			if (line.trim().length > 0) {
				this.matchStaticSectionContents(line).forEach(match => {
					const tokenizer = new Tokenizer();
					let tokens = tokenizer.process(match, lineNumber,
						line.indexOf(match));

					this._errors = [
						...this._errors,
						...new EglStaticSectionParser(
							tokens,
							textDocument,
							connection,
							eglAst,
							settings
						).parse()
					];
				});
			}
			lineNumber++;
		});

		// Tokenize expressions

		lines.forEach(line => {});

		return this._errors;
	}

	matchStaticSectionContents(content: string): Array<string> {
		const sectionContentsRegex = /\[%=(.*?)%/gm;
		let match;
		let matches = [];
		while (match = sectionContentsRegex.exec(content)) {
			matches.push(match[1]);
		}
		return matches;
	}
}





const eglSections = new EglSections();

const tokenize = new Tokenizer();

console.log(JSON.stringify(tokenize.process("mainPackage")));
// let result = tokenize.process(`

// [%=getBotWrittenWarning() %]
// package [%=mainPackage%].services;

// import [%=mainPackage%].entities.AbstractEntityAudit;
// import [%=mainPackage%].entities.[%=entityNamePascalCase%]Entity;
// import [%=mainPackage%].entities.[%=entityNamePascalCase%]EntityAudit;
// import [%=mainPackage%].entities.Q[%=entityNamePascalCase%]Entity;
// import [%=mainPackage%].repositories.[%=entityNamePascalCase%]Repository;
// import [%=mainPackage%].graphql.utils.Where;
// import [%=mainPackage%].graphql.utils.AuditQueryType;
// import [%=mainPackage%].services.utils.QuerydslUtils;
// import [%=mainPackage%].configs.security.auditing.CustomRevisionEntity;

// `);

// console.log(result);

/*
TODO: check for broken static sections
TODO: Check for broken dynamic sections
TODO: Check for broken comments

TODO: Handle addition operation in static section.
TODO: handle multiline static section.
TODO: Handle new lines in static content.
*/