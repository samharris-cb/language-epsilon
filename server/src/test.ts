export interface IToken {
	index: number;
	offSet: number;
	line: number;
	value: string;
}

export const Tokens = {
	'SPACE': /\W/,
	'TPS_CLOSE': /%\]/
};

export class Tokenizer {
	parse(text: string): Array<IToken> {
		let tokens = [];

		// Split lines
		const lines = text.split('\n');
		let currentLine = 0;

		lines.forEach(line => {
			
			currentLine++;
		});

		return tokens;
	}
}