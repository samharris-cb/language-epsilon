"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EglStaticSectionParser = exports.DiagnosticType = exports.DynamicSectionActionCode = exports.StaticSectionActionCode = void 0;
const vscode_languageserver_1 = require("vscode-languageserver");
const lintingError_1 = require("../interfaces/lintingError");
const sources_enum_1 = require("../interfaces/sources.enum");
const tokens_1 = require("../interfaces/tokens");
const egl_ast_1 = require("./egl.ast");
var StaticSectionActionCode;
(function (StaticSectionActionCode) {
    StaticSectionActionCode["StartSpace"] = "static-section-start-space";
    StaticSectionActionCode["EndSpace"] = "static-section-end-space";
})(StaticSectionActionCode = exports.StaticSectionActionCode || (exports.StaticSectionActionCode = {}));
var DynamicSectionActionCode;
(function (DynamicSectionActionCode) {
    DynamicSectionActionCode["StartSpace"] = "dynamic-section-start-space";
    DynamicSectionActionCode["EndSpace"] = "dynamic-section-end-space";
})(DynamicSectionActionCode = exports.DynamicSectionActionCode || (exports.DynamicSectionActionCode = {}));
var DiagnosticType;
(function (DiagnosticType) {
    DiagnosticType[DiagnosticType["STYLE"] = 0] = "STYLE";
    DiagnosticType[DiagnosticType["SYNTAX"] = 1] = "SYNTAX";
})(DiagnosticType = exports.DiagnosticType || (exports.DiagnosticType = {}));
/**
 * To operate on a single static section
 */
class EglStaticSectionParser {
    constructor(tokenStream, textDocument, connection, eglAst, settings) {
        this._tokenStream = tokenStream;
        this._textDocument = textDocument;
        this._connection = connection;
        this._eglAst = eglAst;
        this._settings = settings;
    }
    parse() {
        this._errors = [];
        this.log(JSON.stringify(this._tokenStream.getTokenNames()));
        const nonWhiteSpaceTokenCount = this._tokenStream.getTokenNames().filter(name => name != "SPACE").length;
        if (nonWhiteSpaceTokenCount < 1) {
            this.raiseError("Empty static section", DiagnosticType.SYNTAX);
            return this._errors;
        }
        if (this._tokenStream.checkToken(tokens_1.TokenKeys.SPACE, this._connection)) {
            this._tokenStream.consumeToken();
        }
        else {
            this.raiseWarning("Missing space between start of static section and section contents.", DiagnosticType.STYLE, StaticSectionActionCode.StartSpace);
        }
        this.parseContent();
        if (!this._tokenStream.checkToken(tokens_1.TokenKeys.SPACE, this._connection)) {
            this.raiseWarning("Missing space between the static section contents and the end of the section.", DiagnosticType.STYLE, StaticSectionActionCode.EndSpace);
        }
        else {
            this._tokenStream.consumeToken();
        }
        return this._errors;
    }
    parseContent() {
        this.traceStep("Parse content");
        while (this._tokenStream.nextToken() != tokens_1.EOF_TOKEN) {
            if (this._tokenStream.currentTokenKey() === tokens_1.TokenKeys.SPACE) {
                this._tokenStream.consumeToken();
                this.parseContent();
            }
            else {
                this.parseExpression();
            }
        }
    }
    parseExpression() {
        this.logStep("Parse expression");
        if (this._tokenStream.checkTokens([tokens_1.TokenKeys.IDENTIFIER, tokens_1.TokenKeys.KW_PROTECTED])) {
            if (this._tokenStream.checkNextTwoTokens([tokens_1.TokenKeys.OP_PLUS])) {
                this.parseOperation();
            }
            else if (this._tokenStream.checkNextTokens([tokens_1.TokenKeys.L_BRACKET])) {
                this.parseMethod();
            }
            else {
                this.parseIdentifier();
            }
        }
        else {
            this.raiseError(`Unexpected token -> Expected <expression> but got ${this._tokenStream.currentToken().value}`, DiagnosticType.SYNTAX);
            this._tokenStream.consumeToken();
        }
    }
    parseIdentifier() {
        this.traceStep("Parse identifier");
        if (this._tokenStream.checkNextTokens([tokens_1.TokenKeys.L_BRACKET])) {
            this.parseMethod();
        }
        else if (this._tokenStream.nextTokenKey() == tokens_1.TokenKeys.DOT) {
            this.parseAttributeAccess();
        }
        else {
            this._tokenStream.consumeToken();
        }
    }
    parseMethod() {
        this.logStep("Parse method");
        if (this._tokenStream.currentTokenKey() == tokens_1.TokenKeys.KW_PROTECTED) {
            this.parseProtected();
        }
        else {
            this.consumeSpecificToken(tokens_1.TokenKeys.IDENTIFIER);
            this.consumeSpecificToken(tokens_1.TokenKeys.L_BRACKET);
            if (this._tokenStream.currentTokenKey() === tokens_1.TokenKeys.IDENTIFIER) {
                this.parseExpression();
            }
            // Handle random whitespace
            this.consumeInvalidWhitespace(tokens_1.TokenKeys.R_BRACKET);
            this.consumeSpecificToken(tokens_1.TokenKeys.R_BRACKET, `Unexpected token -> Expected ) but got ${this._tokenStream.currentToken().value}`);
        }
    }
    parseAttributeAccess() {
        this.traceStep("Parse attribute access");
        this.consumeSpecificToken(tokens_1.TokenKeys.IDENTIFIER);
        this.consumeSpecificToken(tokens_1.TokenKeys.DOT);
        // Identifier must follow dot
        if (!this._tokenStream.checkTokens([tokens_1.TokenKeys.IDENTIFIER])) {
            this.raiseError(`Expecting IDENTIFIER but found ${this._tokenStream.currentTokenKey()}`, DiagnosticType.SYNTAX);
        }
    }
    parseProtected() {
        const startLine = this._tokenStream.currentToken().location.line;
        const start = this._tokenStream.currentToken().location.start;
        this.consumeSpecificToken(tokens_1.TokenKeys.KW_PROTECTED);
        this.consumeSpecificToken(tokens_1.TokenKeys.L_BRACKET);
        this.consumeInvalidWhitespace(tokens_1.TokenKeys.KW_OUT);
        this.consumeSpecificToken(tokens_1.TokenKeys.KW_OUT);
        this.consumeSpecificToken(tokens_1.TokenKeys.COMMA);
        this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE);
        const id = this.consumeString();
        this.consumeSpecificToken(tokens_1.TokenKeys.COMMA);
        this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE);
        this.consumeString();
        this.consumeInvalidWhitespace(tokens_1.TokenKeys.R_BRACKET);
        this.consumeSpecificToken(tokens_1.TokenKeys.R_BRACKET);
        const end = this._tokenStream.currentToken().location.end;
        const pr = new egl_ast_1.ProtectedRegion(id, vscode_languageserver_1.Position.create(startLine, start), vscode_languageserver_1.Position.create(startLine, end));
        const prs = this._eglAst.addProtectedRegion(pr);
        if (prs.length > 1 && this._settings.lintDuplicateProtectedRegionIds) {
            prs.forEach(x => {
                this.raisePrError(x, prs);
            });
        }
    }
    parseOperation() {
        this.traceStep("Parse operation");
        this.consumeSpecificToken(tokens_1.TokenKeys.IDENTIFIER);
        this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE, "Missing space before operator");
        if (this._tokenStream.checkTokens([tokens_1.TokenKeys.OP_PLUS])) {
            this._tokenStream.consumeToken();
            this.consumeSpecificTokenStyle(tokens_1.TokenKeys.SPACE, "Missing space after operator");
            this.parseExpression();
        }
        else {
            this.raiseError(`Unknown token ${this._tokenStream.currentToken().name}`, DiagnosticType.SYNTAX);
        }
    }
    // -----------------------------------------------------------------------------
    //  ██████╗ ██████╗ ███╗   ██╗███████╗██╗   ██╗███╗   ███╗███████╗██████╗ ███████╗
    // ██╔════╝██╔═══██╗████╗  ██║██╔════╝██║   ██║████╗ ████║██╔════╝██╔══██╗██╔════╝
    // ██║     ██║   ██║██╔██╗ ██║███████╗██║   ██║██╔████╔██║█████╗  ██████╔╝███████╗
    // ██║     ██║   ██║██║╚██╗██║╚════██║██║   ██║██║╚██╔╝██║██╔══╝  ██╔══██╗╚════██║
    // ╚██████╗╚██████╔╝██║ ╚████║███████║╚██████╔╝██║ ╚═╝ ██║███████╗██║  ██║███████║
    //  ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝ ╚═════╝ ╚═╝     ╚═╝╚══════╝╚═╝  ╚═╝╚══════╝
    //
    // -----------------------------------------------------------------------------
    /**
     * Return string contents and as it is consumed.
     */
    consumeString() {
        this.consumeSpecificTokenStyleWithAlternatives(tokens_1.TokenKeys.SINGLE_QUOTE, `Expected single quote, found ${this._tokenStream.currentToken().value}`, [tokens_1.TokenKeys.DOUBLE_QUOTE, tokens_1.TokenKeys.SINGLE_QUOTE]);
        const expectedTerminator = this._tokenStream.previousTokenKey();
        let contents = [];
        while (!this._tokenStream.checkTokens([tokens_1.TokenKeys.EOF, expectedTerminator])) {
            contents.push(this._tokenStream.currentToken());
            this._tokenStream.consumeToken();
        }
        this.consumeSpecificToken(expectedTerminator);
        const reducer = (accumulator, currentValue) => accumulator + currentValue.value;
        return contents.reduce(reducer, contents.length);
    }
    consumeInvalidWhitespace(expectedToken) {
        if (this._tokenStream.currentTokenKey() === tokens_1.TokenKeys.SPACE) {
            this.raiseWarning(`Expected ${expectedToken} but got SPACE`, DiagnosticType.STYLE);
            this.consumeWhitespace();
        }
    }
    consumeWhitespace() {
        if (this._tokenStream.currentTokenKey() === tokens_1.TokenKeys.SPACE) {
            this.traceStep("Consume whitespace");
            this._tokenStream.consumeToken();
        }
    }
    consumeSpecificToken(tokenKey, message) {
        this.traceStep(`Consume ${tokenKey}`);
        if (!this._tokenStream.consumeSpecificToken(tokenKey)) {
            if (message) {
                this.raiseError(message, DiagnosticType.SYNTAX);
            }
            else {
                this.raiseError(`Unexpected token, expected ${tokenKey} but got ${this._tokenStream.currentToken().value}`, DiagnosticType.SYNTAX);
            }
        }
    }
    consumeSpecificTokenStyleWithAlternatives(tokenKey, message, validAlternatives) {
        if (validAlternatives) {
            if (this._tokenStream.checkTokens(validAlternatives)) {
                this.consumeSpecificTokenStyle(tokenKey, message);
            }
            else {
                this.raiseError(message, DiagnosticType.SYNTAX);
            }
        }
        else {
            this.consumeSpecificTokenStyle(tokenKey, message);
        }
    }
    consumeSpecificTokenStyle(tokenKey, message) {
        if (!this._tokenStream.consumeSpecificToken(tokenKey)) {
            if (message) {
                this.raiseWarning(message, DiagnosticType.STYLE);
            }
            else {
                this.raiseWarning(`Unexpected token, expected ${tokenKey} but got ${this._tokenStream.currentToken().value}`, DiagnosticType.STYLE);
            }
        }
    }
    // ----------------------------------------------------------------------------
    // ██████╗ ██╗ █████╗  ██████╗ ███╗   ██╗ ██████╗ ███████╗████████╗██╗ ██████╗
    // ██╔══██╗██║██╔══██╗██╔════╝ ████╗  ██║██╔═══██╗██╔════╝╚══██╔══╝██║██╔════╝
    // ██║  ██║██║███████║██║  ███╗██╔██╗ ██║██║   ██║███████╗   ██║   ██║██║
    // ██║  ██║██║██╔══██║██║   ██║██║╚██╗██║██║   ██║╚════██║   ██║   ██║██║
    // ██████╔╝██║██║  ██║╚██████╔╝██║ ╚████║╚██████╔╝███████║   ██║   ██║╚██████╗
    // ╚═════╝ ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚══════╝   ╚═╝   ╚═╝ ╚═════╝
    //
    // ----------------------------------------------------------------------------
    raisePrError(pr, prs) {
        if (!pr.Flagged) {
            const relatedDiagnosticInformation = prs
                .filter(x => x.getUniqueId() != pr.getUniqueId())
                .map(pr => {
                return vscode_languageserver_1.DiagnosticRelatedInformation.create({
                    range: {
                        start: pr.startPosition,
                        end: pr.endPosition
                    },
                    uri: this._textDocument.uri
                }, "Duplicate protected region");
            });
            this.raiseDiagnosticDetail("Duplicate protected region ID", vscode_languageserver_1.DiagnosticSeverity.Error, DiagnosticType.SYNTAX, pr.startPosition.line, pr.startPosition.character, pr.endPosition.character, null, relatedDiagnosticInformation);
        }
        pr.Flagged = true;
    }
    raiseWarning(errorMessage, type, code) {
        this.raiseDiagnostic(errorMessage, vscode_languageserver_1.DiagnosticSeverity.Warning, type, code);
    }
    raiseError(errorMessage, type) {
        this.raiseDiagnostic(errorMessage, vscode_languageserver_1.DiagnosticSeverity.Error, type);
    }
    raiseDiagnostic(message, severity, type, code) {
        this.log(message);
        const currentToken = this._tokenStream.currentToken();
        this.raiseDiagnosticDetail(message, severity, type, currentToken.location.line, currentToken.location.start, currentToken.location.end, code);
    }
    raiseDiagnosticDetail(message, severity, type, line, start, end, code, relatedInformation) {
        this.log(`Number of errors ${this._errors.length + 1}`);
        if (this._settings.maxNumberOfProblems < this._errors.length + 1) {
            this.log(`Maximum errors reached ${this._settings.maxNumberOfProblems}, discarding the others.`);
            return;
        }
        if (!this._settings.lintStyle && type === DiagnosticType.STYLE) {
            this.log("Lint style is off, skipping");
            return;
        }
        const error = {
            severity: severity,
            range: {
                start: vscode_languageserver_1.Position.create(line, start),
                end: vscode_languageserver_1.Position.create(line, end)
            },
            message: message,
            source: sources_enum_1.Source.epsilonLanguageEgl,
        };
        if (relatedInformation) {
            error.relatedInformation = relatedInformation;
        }
        if (code) {
            error.code = code;
        }
        this._errors.push(error);
    }
    log(message) {
        if (this._settings.logLevel != lintingError_1.LogLevel.NONE) {
            this._connection.console.log(`EglStaticSectionParser -> ${message}`);
        }
    }
    logStep(message) {
        this.log(`PARSE -> ${message}`);
    }
    traceStep(message) {
        this.trace(`PARSE -> ${message}`);
    }
    trace(message) {
        this._connection.tracer.log(`EglStaticSectionParser -> ${message}`);
    }
}
exports.EglStaticSectionParser = EglStaticSectionParser;
//# sourceMappingURL=egl-static-section-parser.js.map