"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
require("mocha");
const tokens_1 = require("../interfaces/tokens");
const egl_string_utils_1 = require("./egl-string-utils");
const token_stream_1 = require("./token-stream");
const tokenizer_1 = require("./tokenizer");
describe("Match static section", () => {
    const tokenizer = new egl_string_utils_1.EglSections();
    it("Match single identifier in static section", () => {
        const line = "import [%=mainPackage%].entities.AbstractEntityAudit;";
        const result = tokenizer.matchStaticSectionContents(line)[0];
        const expectedResult = "mainPackage";
        chai_1.expect(result).to.equal(expectedResult);
    });
    it("Match multiple static sections in the same line", () => {
        const line = "import [%=mainPackage%].entities.[%= test() %];";
        const result = tokenizer.matchStaticSectionContents(line);
        const expectedResult = ["mainPackage", " test() "];
        chai_1.expect(result[0]).to.equal(expectedResult[0]);
        chai_1.expect(result[1]).to.equal(expectedResult[1]);
    });
});
describe("Process static section matches", () => {
    const tokenizer = new tokenizer_1.Tokenizer();
    it("Process single identifier in static section", () => {
        const match = "mainPackage";
        const lineNumber = 4;
        const start = 4;
        const result = tokenizer.process(match, lineNumber, start);
        chai_1.expect(result.currentToken().name).to.equal('IDENTIFIER');
        chai_1.expect(result.currentToken().value).to.equal(match);
        chai_1.expect(result.currentToken().location.start).to.equal(start);
        chai_1.expect(result.currentToken().location.end).to.equal(start + 11);
        chai_1.expect(result.currentToken().location.line).to.equal(lineNumber);
    });
    describe('Token stream', () => {
        const tokens = [
            {
                name: "SPACE",
                value: " ",
                location: {
                    start: 0,
                    end: 1,
                    line: 1
                }
            },
            {
                name: "L_BRACKET",
                value: "(",
                location: {
                    start: 1,
                    end: 2,
                    line: 1
                }
            }
        ];
        it('Return current token', () => {
            const tokenStream = new token_stream_1.TokenStream(tokens);
            const result = tokenStream.currentToken();
            chai_1.expect(result).to.equal(tokens[0]);
        });
        it('Get next token', () => {
            const tokenStream = new token_stream_1.TokenStream(tokens);
            const result = tokenStream.nextToken();
            chai_1.expect(result).to.equal(tokens[1]);
        });
        it("Consume token", () => {
            const tokenStream = new token_stream_1.TokenStream(tokens);
            const result = tokenStream.consumeToken();
            chai_1.expect(result).to.equal(tokens[0]);
        });
        it("Consume token and check current", () => {
            const tokenStream = new token_stream_1.TokenStream(tokens);
            tokenStream.consumeToken();
            const result = tokenStream.currentToken();
            chai_1.expect(result).to.equal(tokens[1]);
        });
        it("Check previous for first token", () => {
            const tokenStream = new token_stream_1.TokenStream(tokens);
            const result = tokenStream.previousToken();
            chai_1.expect(result).to.equal(tokens_1.EOF_TOKEN);
        });
        it("Check next for last token", () => {
            const tokenStream = new token_stream_1.TokenStream(tokens);
            tokenStream.consumeToken();
            const result = tokenStream.nextToken();
            chai_1.expect(result).to.equal(tokens_1.EOF_TOKEN);
        });
        // it("checkNextSet for two tokens", () => {
        // 	const tokenStream = new TokenStream(tokens);
        // 	tokenStream.consumeToken();
        // 	const result = tokenStream.nextToken();
        // 	expect(result).to.equal(SpecialTokens.EOF);
        // });
    });
    describe('EglStaticSectionParser', () => {
        const tokens = [{
                name: 'IDENTIFIER',
                value: 'mainPackage',
                location: { start: 1, end: 12, line: 1 }
            },
            {
                name: 'L_BRACKET',
                value: '(',
                location: { start: 12, end: 13, line: 1 }
            },
            {
                name: 'R_BRACKET',
                value: ')',
                location: { start: 13, end: 14, line: 1 }
            }];
        const tokenStream = new token_stream_1.TokenStream(tokens);
    });
});
//# sourceMappingURL=egl-tokenizer.spec.js.map