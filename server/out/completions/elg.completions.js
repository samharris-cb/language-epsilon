"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResolveEglCompletion = exports.EglCompletions = void 0;
const vscode_languageserver_types_1 = require("vscode-languageserver-types");
exports.EglCompletions = [
    {
        label: 'cached',
        kind: vscode_languageserver_types_1.CompletionItemKind.Property,
        data: 1,
        insertText: '@cached\n',
        detail: 'Operation Result Caching',
        documentation: 'Caching of the result of the operation, simply add this annotation to any operation for'
            + 'the result to be cached'
    },
    {
        label: 'operation',
        kind: vscode_languageserver_types_1.CompletionItemKind.Keyword,
        data: 2,
        detail: "Operation",
        documentation: 'Definition of the kind of objects in with this operation is applicable (context),'
            + 'a name, parameters and optionally a return type.'
    },
];
function ResolveEglCompletion(item) {
    return item;
}
exports.ResolveEglCompletion = ResolveEglCompletion;
//# sourceMappingURL=elg.completions.js.map