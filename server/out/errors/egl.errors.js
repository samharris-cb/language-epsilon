"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DUPLICATE_PR = exports.ERRORS = void 0;
const vscode_languageserver_1 = require("vscode-languageserver");
exports.ERRORS = [
    {
        match: /^(\s?%\])/gm,
        message: "Template closing character should be at the end of the previous line",
        severity: vscode_languageserver_1.DiagnosticSeverity.Warning
    },
];
exports.DUPLICATE_PR = {
    match: /\[%\= ?protected\(out, ["|'](.+)["|'], ["|']["|']\) ?%\]/gm,
    message: 'Duplicate protected region ID',
    severity: vscode_languageserver_1.DiagnosticSeverity.Error,
};
//# sourceMappingURL=egl.errors.js.map