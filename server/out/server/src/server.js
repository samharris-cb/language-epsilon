"use strict";
/* --------------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * ------------------------------------------------------------------------------------------ */
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_languageserver_1 = require("vscode-languageserver");
const egx_completion_1 = require("./completions/egx.completion");
const elg_completions_1 = require("./completions/elg.completions");
const eol_completions_1 = require("./completions/eol.completions");
const egl_1 = require("./egl");
const egx_1 = require("./egx");
const eol_1 = require("./eol");
const languages_enum_1 = require("./interfaces/languages.enum");
const lintingError_1 = require("./interfaces/lintingError");
const utils_1 = require("./utils");
// Create a connection for the server. The connection uses Node's IPC as a transport.
// Also include all preview / proposed LSP features.
let connection = vscode_languageserver_1.createConnection(vscode_languageserver_1.ProposedFeatures.all);
// Create a simple text document manager. The text document manager
// supports full document sync only
let documents = new vscode_languageserver_1.TextDocuments();
let hasConfigurationCapability = false;
let hasWorkspaceFolderCapability = false;
let hasDiagnosticRelatedInformationCapability = false;
connection.onInitialize((params) => {
    let capabilities = params.capabilities;
    // Does the client support the `workspace/configuration` request?
    // If not, we will fall back using global settings
    hasConfigurationCapability = !!(capabilities.workspace && !!capabilities.workspace.configuration);
    hasWorkspaceFolderCapability = !!(capabilities.workspace && !!capabilities.workspace.workspaceFolders);
    hasDiagnosticRelatedInformationCapability =
        !!(capabilities.textDocument &&
            capabilities.textDocument.publishDiagnostics &&
            capabilities.textDocument.publishDiagnostics.relatedInformation);
    return {
        capabilities: {
            textDocumentSync: documents.syncKind,
            // Tell the client that the server supports code completion
            completionProvider: {
                resolveProvider: true
            }
        }
    };
});
connection.onInitialized(() => {
    if (hasConfigurationCapability) {
        // Register for all configuration changes.
        connection.client.register(vscode_languageserver_1.DidChangeConfigurationNotification.type, undefined);
    }
    if (hasWorkspaceFolderCapability) {
        connection.workspace.onDidChangeWorkspaceFolders(_event => {
            connection.console.log('Workspace folder change event received.');
        });
    }
});
// The global settings, used when the `workspace/configuration` request is not supported by the client.
// Please note that this is not the case when using this server with the client provided in this example
// but could happen with other clients.
const defaultSettings = {
    maxNumberOfProblems: 1000,
    lintStyle: true,
    lintDuplicateProtectedRegionIds: true,
    logLevel: lintingError_1.LogLevel.NONE
};
let globalSettings = defaultSettings;
// Cache the settings of all open documents
let documentSettings = new Map();
connection.onDidChangeConfiguration(change => {
    if (hasConfigurationCapability) {
        // Reset all cached document settings
        documentSettings.clear();
    }
    else {
        globalSettings = ((change.settings.epsilonLanguagesSettings || defaultSettings));
    }
    // Revalidate all open text documents
    documents.all().forEach((document) => {
        switch (document.languageId) {
            case 'language-eol':
                eol_1.validateEOLDocument(document, connection, hasDiagnosticRelatedInformationCapability);
                break;
            case 'language-egl':
                egl_1.validateEGLDocument(document, connection, hasDiagnosticRelatedInformationCapability);
                break;
            case 'language-egx':
                egx_1.validateEGXDocument(document, connection, hasDiagnosticRelatedInformationCapability);
                break;
            default:
                return;
        }
    });
});
function getDocumentSettings(resource) {
    if (!hasConfigurationCapability) {
        return Promise.resolve(globalSettings);
    }
    let result = documentSettings.get(resource);
    if (!result) {
        result = connection.workspace.getConfiguration({
            scopeUri: resource,
            section: 'languageServerEpsilon'
        });
        documentSettings.set(resource, result);
    }
    return result;
}
exports.getDocumentSettings = getDocumentSettings;
// Only keep settings for open documents
documents.onDidClose(e => {
    documentSettings.delete(e.document.uri);
});
// The content of a text document has changed. This event is emitted
// when the text document first opened or when its content has changed.
documents.onDidChangeContent(change => {
    connection.console.log(`Change detected for language ID -> ${change.document.languageId}`);
    triggerParse(change);
});
documents.onDidSave(save => {
    connection.console.log(`Save detected for language ID -> ${save.document.languageId}`);
    // triggerParse(save);
});
connection.onDidChangeWatchedFiles(_change => {
    // Monitored files have change in VSCode
    connection.console.log('We received a file change event');
});
function triggerParse(change) {
    switch (change.document.languageId) {
        case languages_enum_1.Language.EOL:
            eol_1.validateEOLDocument(change.document, connection, hasDiagnosticRelatedInformationCapability);
            break;
        case languages_enum_1.Language.EGL:
            egl_1.validateEGLDocument(change.document, connection, hasDiagnosticRelatedInformationCapability);
            break;
        case languages_enum_1.Language.EGX:
            egx_1.validateEGXDocument(change.document, connection, hasDiagnosticRelatedInformationCapability);
            break;
        default:
            return;
    }
}
// -----------------------------------------------------------------------------
//  ██████╗ ██████╗ ███╗   ███╗██████╗ ██╗     ███████╗████████╗██╗ ██████╗ ███╗   ██╗███████╗
// ██╔════╝██╔═══██╗████╗ ████║██╔══██╗██║     ██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
// ██║     ██║   ██║██╔████╔██║██████╔╝██║     █████╗     ██║   ██║██║   ██║██╔██╗ ██║███████╗
// ██║     ██║   ██║██║╚██╔╝██║██╔═══╝ ██║     ██╔══╝     ██║   ██║██║   ██║██║╚██╗██║╚════██║
// ╚██████╗╚██████╔╝██║ ╚═╝ ██║██║     ███████╗███████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║
//  ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚═╝     ╚══════╝╚══════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝
//
// -----------------------------------------------------------------------------
const CompletionsMap = {
    [languages_enum_1.Language.EOL]: eol_completions_1.EolCompletions,
    [languages_enum_1.Language.EGX]: egx_completion_1.EgxCompletions,
    [languages_enum_1.Language.EGL]: elg_completions_1.EglCompletions
};
let currentLanguage;
connection.onCompletion((_textDocumentPosition) => {
    const uri = _textDocumentPosition.textDocument.uri;
    currentLanguage = utils_1.UriUtils.getLanguage(uri);
    return CompletionsMap[currentLanguage];
});
// This handler resolves additional information for the item selected in
// the completion list.
const ResolutionMap = {
    [languages_enum_1.Language.EOL]: eol_completions_1.ResolveEolCompletion,
    [languages_enum_1.Language.EGX]: egx_completion_1.ResolveEgxCompletion,
    [languages_enum_1.Language.EGL]: elg_completions_1.ResolveEglCompletion
};
connection.onCompletionResolve((item) => {
    return ResolutionMap[currentLanguage](item);
});
// Make the text document manager listen on the connection
// for open, change and close text document events
documents.listen(connection);
// Listen on the connection
connection.listen();
//# sourceMappingURL=server.js.map