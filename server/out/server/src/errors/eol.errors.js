"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_languageserver_1 = require("vscode-languageserver");
exports.ERRORS = [
    {
        match: /(".+")/gm,
        message: `Use single quotes`,
        severity: vscode_languageserver_1.DiagnosticSeverity.Warning
    }
];
//# sourceMappingURL=eol.errors.js.map