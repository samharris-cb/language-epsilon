"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_languageserver_1 = require("vscode-languageserver");
const server_1 = require("./server");
function validateEGXDocument(textDocument, connection, hasDiagnosticRelatedInformationCapability) {
    return __awaiter(this, void 0, void 0, function* () {
        // In this simple example we get the settings for every validate run.
        let settings = yield server_1.getDocumentSettings(textDocument.uri);
        // The validator creates diagnostics for all uppercase words length 2 and more
        let text = textDocument.getText();
        let pattern = /(\[[^\*\%])/gm;
        let m;
        let problems = 0;
        let diagnostics = [];
        // Handle EGL
        while ((m = pattern.exec(text)) && problems < settings.maxNumberOfProblems) {
            problems++;
            let diagnosic = {
                severity: vscode_languageserver_1.DiagnosticSeverity.Warning,
                range: {
                    start: textDocument.positionAt(m.index),
                    end: textDocument.positionAt(m.index + m[0].length)
                },
                message: `${m[0]} is an incorrect template start.`,
                source: 'Epsilon Language'
            };
            if (hasDiagnosticRelatedInformationCapability) {
                diagnosic.relatedInformation = [
                    {
                        location: {
                            uri: textDocument.uri,
                            range: Object.assign({}, diagnosic.range)
                        },
                        message: 'Did you forget to start your template string or comment?'
                    },
                    {
                        location: {
                            uri: textDocument.uri,
                            range: Object.assign({}, diagnosic.range)
                        },
                        message: 'Make sure you use `[*` or `[%` or `[%='
                    }
                ];
            }
            diagnostics.push(diagnosic);
        }
        // Send the computed diagnostics to VSCode.
        connection.sendDiagnostics({ uri: textDocument.uri, diagnostics });
    });
}
exports.validateEGXDocument = validateEGXDocument;
//# sourceMappingURL=egx.js.map