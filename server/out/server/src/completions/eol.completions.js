"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_languageserver_types_1 = require("vscode-languageserver-types");
exports.EolCompletions = [
    {
        label: '@cached\n',
        kind: vscode_languageserver_types_1.CompletionItemKind.Keyword,
        data: 1,
    },
    {
        label: 'operation ',
        kind: vscode_languageserver_types_1.CompletionItemKind.Class,
        data: 2
    },
    {
        label: 'new Map()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Constructor,
        data: 4
    },
    {
        label: 'asBag()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 5
    },
    {
        label: 'asBoolean()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 6
    },
    {
        label: 'asInteger()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 7
    },
    {
        label: 'asOrderedSet()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 8
    },
    {
        label: 'asReal()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 9
    },
    {
        label: 'asDouble()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 10
    },
    {
        label: 'asFloat()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 11
    },
    {
        label: 'asSequence()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 12
    },
    {
        label: 'asSet()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 13
    },
    {
        label: 'asString()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 14
    },
    {
        label: 'isDefined()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 20
    },
    {
        label: 'isUndefined()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 23
    },
    {
        label: 'owningModel()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 24
    },
    {
        label: 'type()',
        kind: vscode_languageserver_types_1.CompletionItemKind.Function,
        data: 27
    },
];
function ResolveEolCompletion(item) {
    switch (item.data) {
        case 1:
            item.detail = 'Operation Result Caching';
            item.documentation =
                `Caching of the result of the operation, simply add this annotation to any operation for the result to be cached`;
            break;
        case 2:
            item.detail = 'Operation';
            item.documentation =
                `Definition of the kind of objects in with this operation is applicable (context), a name, parameters and optionally a return type.`;
            break;
            break;
        case 4:
            item.detail = 'new Map Object';
            item.documentation = '';
            break;
        case 5:
            item.detail = 'asBag(): Bag';
            item.documentation = 'Returns a new Bag containing the object';
            break;
        case 6:
            item.detail = 'asBoolean(): Boolean';
            item.documentation = 'Returns a Boolean based on the string representation of the object.  If the string representation is not of an acceptable format, an error is raised';
            break;
        case 7:
            item.detail = 'asInteger(): Integer';
            item.documentation = 'Returns an Integer based on the string representation of the object. If the string representation is not of an acceptable format, an error is raised';
            break;
        case 8:
            item.detail = 'asOrderedSet() : OrderedSet';
            item.documentation = 'Returns a new OrderedSet containing the object';
            break;
        case 9:
            item.detail = 'asReal() : Real';
            item.documentation = 'Returns a Real based on the string representation of the object. If the string representation is not of an acceptable format, an error is raised';
            break;
        case 10:
            item.detail = 'asDouble() : Double';
            item.documentation = 'Returns a Java Double  based  on  the  string  representation of the object. If the string representation is not of an acceptable format, an error is raised';
            break;
        case 11:
            item.detail = 'asFloat(): Float';
            item.documentation = 'Returns a Java Float based on the string representation of the object. If the string representation is not of an acceptable format, an error is raised';
            break;
        case 12:
            item.detail = 'asSequence() : Sequence';
            item.documentation = 'Returns a new Sequence containing the object';
            break;
        case 13:
            item.detail = 'asSet() : Set';
            item.documentation = 'Returns a new Set containing the object';
            break;
        case 14:
            item.detail = 'asString() : String';
            item.documentation = 'Returns a string representation of the object';
            break;
        case 20:
            item.detail = 'ifDefined(): Any';
            item.documentation = 'Returns true if the object is defined and false otherwise.';
            break;
        case 23:
            item.detail = 'isUndefined(): Boolean';
            item.documentation = 'Returns true if the object is undefined and false otherwise';
            break;
        case 24:
            ;
            item.detail = 'owningModel(): Model';
            item.documentation = 'Returns the model that contains this object or an undefined value otherwise';
            break;
        case 27:
            item.detail = 'type(): Type';
            item.documentation = 'Returns the type of the object.  The EOL type sys-tem is illustrated in Figure.';
            break;
    }
    return item;
}
exports.ResolveEolCompletion = ResolveEolCompletion;
//# sourceMappingURL=eol.completions.js.map