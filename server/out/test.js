"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tokenizer = exports.Tokens = void 0;
exports.Tokens = {
    'SPACE': /\W/,
    'TPS_CLOSE': /%\]/
};
class Tokenizer {
    parse(text) {
        let tokens = [];
        // Split lines
        const lines = text.split('\n');
        let currentLine = 0;
        lines.forEach(line => {
            currentLine++;
        });
        return tokens;
    }
}
exports.Tokenizer = Tokenizer;
//# sourceMappingURL=test.js.map