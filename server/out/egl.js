"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateEGLDocument = void 0;
const egl_errors_1 = require("./errors/egl.errors");
const egl_string_utils_1 = require("./helpers/egl-string-utils");
const sources_enum_1 = require("./interfaces/sources.enum");
const server_1 = require("./server");
function validateEGLDocument(textDocument, connection, hasDiagnosticRelatedInformationCapability) {
    return __awaiter(this, void 0, void 0, function* () {
        // In this simple example we get the settings for every validate run.
        let settings = yield server_1.getDocumentSettings(textDocument.uri);
        // The validator creates diagnostics for all uppercase words length 2 and more
        let text = textDocument.getText();
        let m;
        let problems = 0;
        let diagnostics = [];
        const lintSettings = Object.assign({}, settings);
        diagnostics = new egl_string_utils_1.EglSections().process(text, textDocument, connection, lintSettings);
        if (settings.lintStyle) {
            egl_errors_1.ERRORS.forEach(error => {
                while ((m = error.match.exec(text)) && problems < settings.maxNumberOfProblems) {
                    problems++;
                    let diagnostic = {
                        severity: error.severity,
                        range: {
                            start: textDocument.positionAt(m.index),
                            end: textDocument.positionAt(m.index + m[0].length)
                        },
                        message: error.message,
                        source: sources_enum_1.Source.epsilonLanguageEgl
                    };
                    diagnostics.push(diagnostic);
                }
            });
        }
        // Send the computed diagnostics to VSCode.
        connection.sendDiagnostics({ uri: textDocument.uri, diagnostics });
    });
}
exports.validateEGLDocument = validateEGLDocument;
//# sourceMappingURL=egl.js.map