# Language Epsilon - WORK IN PROGRESS
A set of tools for the [Eclipse Epsilon](https://www.eclipse.org/epsilon/) set of languages.

## Features
- Syntax highlighting for EOL, EGX and EGL
- Snippets for EOL, EGX and EGL
- Opinionated linting implemented using a basic language server
- Basic formatter for EGL
- Code actions for linting error resolution for EGL
- EOL autocomplete


## Future Features
- More complex linting
	- Syntax errors etc
