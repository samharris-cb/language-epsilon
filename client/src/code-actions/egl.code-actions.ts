import { CancellationToken, CodeAction, CodeActionContext, CodeActionKind, CodeActionProvider, Diagnostic, Position, Range, Selection, TextDocument, WorkspaceEdit } from "vscode";

const COMMAND = "code-actions-quickfix.fix-white-space";

export const StaticSectionActionCode = {
	StartSpace: "static-section-start-space",
	EndSpace: "static-section-end-space"
};

const MissingWhiteSpaceCommandLabel = "Add missing white space";

export class FixWhiteSpace implements CodeActionProvider {
	public static readonly providedCodeActionKinds = [CodeActionKind.QuickFix];

	provideCodeActions(
		document: TextDocument,
		range: Range | Selection,
		context: CodeActionContext,
		token: CancellationToken
	): CodeAction[] {
		const fixes = {
			[StaticSectionActionCode.StartSpace]: this.addMissingWhiteSpace,
			[StaticSectionActionCode.EndSpace]: this.addMissingWhiteSpace
		};

		return context.diagnostics
			.filter(diagnostic => fixes[diagnostic.code])
			.map(diagnostic => fixes[diagnostic.code](document, range, diagnostic));
	}

	private addMissingWhiteSpace(document: TextDocument, range: Range, diagnostic: Diagnostic): CodeAction {
		const action = new CodeAction(MissingWhiteSpaceCommandLabel, CodeActionKind.QuickFix);
		action.command = {
			command: COMMAND,
			title: "Auto fix white space at the end of the static section",
			tooltip: "This will attempt to auto fix the white space at the end of the static section."
		};
		action.edit = new WorkspaceEdit();
		const sectionRange = document.getWordRangeAtPosition(
			new Position(range.start.line, range.start.character),
			/\[%=(.*?)%\]/gm
		);

		let content = document.getText(sectionRange);

		// Magic the string
		if (content[3] != ' ' && content[3] != '	') {
			content = content.replace('[%=', '[%= ');
		}

		const expectedSpace = content[content.length - 3];
		if (expectedSpace != " " && expectedSpace != '	') {
			content = content.replace('%]', ' %]');
		}

		action.edit.replace(document.uri, sectionRange, content);
		action.diagnostics = [diagnostic];
		return action;
	}
}
