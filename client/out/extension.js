"use strict";
/* --------------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * ------------------------------------------------------------------------------------------ */
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const vscode_1 = require("vscode");
const vscode_languageclient_1 = require("vscode-languageclient");
const egl_code_actions_1 = require("./code-actions/egl.code-actions");
const format_provider_1 = require("./code-actions/format-provider");
const egl_completions_1 = require("./completions/egl.completions");
const eol_completions_1 = require("./completions/eol.completions");
let client;
function activate(context) {
    // The server is implemented in node
    let serverModule = context.asAbsolutePath(path.join('server', 'out', 'server.js'));
    // The debug options for the server
    // --inspect=6009: runs the server in Node's Inspector mode so VS Code can attach to the server for debugging
    let debugOptions = { execArgv: ['--nolazy', '--inspect=6009'] };
    // If the extension is launched in debug mode then the debug server options are used
    // Otherwise the run options are used
    let serverOptions = {
        run: { module: serverModule, transport: vscode_languageclient_1.TransportKind.ipc },
        debug: {
            module: serverModule,
            transport: vscode_languageclient_1.TransportKind.ipc,
            options: debugOptions
        }
    };
    // Options to control the language client
    let clientOptions = {
        // Register the server for epsilon documents
        documentSelector: [
            { scheme: 'file', language: 'language-egl' },
            { scheme: 'file', language: 'language-eol' },
            { scheme: 'file', language: 'language-egx' }
        ],
        synchronize: {
            // Notify the server about file changes to '.clientrc files contained in the workspace
            fileEvents: vscode_1.workspace.createFileSystemWatcher('**/.clientrc')
        }
    };
    // Create the language client and start the client.
    client = new vscode_languageclient_1.LanguageClient('languageServerEpsilon', 'Language Server Epsilon', serverOptions, clientOptions);
    const languageEgl = "language-egl";
    // Activate any providers
    context.subscriptions.push(
    // Completion
    egl_completions_1.eglCompletionProvider, eol_completions_1.eolCompletionProvider, 
    // Code Actions
    vscode_1.languages.registerCodeActionsProvider(languageEgl, new egl_code_actions_1.FixWhiteSpace(), {
        providedCodeActionKinds: egl_code_actions_1.FixWhiteSpace.providedCodeActionKinds
    }), 
    // Formatter
    vscode_1.languages.registerDocumentFormattingEditProvider(languageEgl, new format_provider_1.EglFormatter()));
    // Start the client. This will also launch the server
    client.start();
}
exports.activate = activate;
function deactivate() {
    if (!client) {
        return undefined;
    }
    return client.stop();
}
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map