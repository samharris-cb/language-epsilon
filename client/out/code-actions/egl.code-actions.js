"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const COMMAND = "code-actions-quickfix.fix-white-space";
exports.StaticSectionActionCode = {
    StartSpace: "static-section-start-space",
    EndSpace: "static-section-end-space"
};
const MissingWhiteSpaceCommandLabel = "Add missing white space";
class FixWhiteSpace {
    provideCodeActions(document, range, context, token) {
        const fixes = {
            [exports.StaticSectionActionCode.StartSpace]: this.addMissingWhiteSpace,
            [exports.StaticSectionActionCode.EndSpace]: this.addMissingWhiteSpace
        };
        return context.diagnostics
            .filter(diagnostic => fixes[diagnostic.code])
            .map(diagnostic => fixes[diagnostic.code](document, range, diagnostic));
    }
    addMissingWhiteSpace(document, range, diagnostic) {
        const action = new vscode_1.CodeAction(MissingWhiteSpaceCommandLabel, vscode_1.CodeActionKind.QuickFix);
        action.command = {
            command: COMMAND,
            title: "Auto fix white space at the end of the static section",
            tooltip: "This will attempt to auto fix the white space at the end of the static section."
        };
        action.edit = new vscode_1.WorkspaceEdit();
        const sectionRange = document.getWordRangeAtPosition(new vscode_1.Position(range.start.line, range.start.character), /\[%=(.*?)%\]/gm);
        let content = document.getText(sectionRange);
        // Magic the string
        if (content[3] != ' ' && content[3] != '	') {
            content = content.replace('[%=', '[%= ');
        }
        const expectedSpace = content[content.length - 3];
        if (expectedSpace != " " && expectedSpace != '	') {
            content = content.replace('%]', ' %]');
        }
        action.edit.replace(document.uri, sectionRange, content);
        action.diagnostics = [diagnostic];
        return action;
    }
}
FixWhiteSpace.providedCodeActionKinds = [vscode_1.CodeActionKind.QuickFix];
exports.FixWhiteSpace = FixWhiteSpace;
//# sourceMappingURL=egl.code-actions.js.map