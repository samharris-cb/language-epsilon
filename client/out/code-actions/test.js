"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StaticSectionActionCode = {
    StartSpace: "static-section-start-space",
    EndSpace: "static-section-end-space"
};
const fixMap = {
    [exports.StaticSectionActionCode.StartSpace]: fixStartSpace,
    [exports.StaticSectionActionCode.EndSpace]: fixEndSpace
};
function fixStartSpace() {
    console.log('saasd');
}
function fixEndSpace() {
    console.log("end");
}
const testValues = [
    {
        code: exports.StaticSectionActionCode.StartSpace.toString()
    },
    {
        code: exports.StaticSectionActionCode.EndSpace.toString()
    }
].filter(diagnostic => fixMap[diagnostic.code]);
console.log(JSON.stringify(testValues));
//# sourceMappingURL=test.js.map