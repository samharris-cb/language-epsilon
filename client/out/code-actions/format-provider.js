"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
class EglFormatter {
    provideDocumentFormattingEdits(document, options, token) {
        return [...this.formatStaticSection(document)];
    }
    formatStaticSection(document) {
        const content = document.getText();
        const contents = content.split("\n");
        let textEdits = [];
        const lineFormatters = [this.formatStaticSectionStart, this.formatStaticSectionEnd];
        contents.forEach((line, index) => {
            lineFormatters.forEach(formatter => {
                // Split as we may have multiple matches
                let matches = line.match(/\[%=(.*?)%\]/gm);
                if (matches) {
                    matches.forEach(item => {
                        let result = formatter(item, index, line.indexOf(item));
                        if (result) {
                            textEdits = [...textEdits, ...result];
                        }
                    });
                }
            });
        });
        return textEdits;
    }
    formatStaticSectionEnd(line, index, characterStart) {
        const edits = [];
        let match = line.match(/(\s*)?%\]/);
        if (match) {
            let matched = match[0];
            let character = characterStart + line.indexOf(matched);
            edits.push(new vscode_1.TextEdit(new vscode_1.Range(new vscode_1.Position(index, character), new vscode_1.Position(index, character + matched.length)), " %]"));
        }
        return edits;
    }
    formatStaticSectionStart(line, index, characterStart) {
        const edits = [];
        let match = line.match(/\[%=(\s+)?/);
        if (match) {
            let matched = match[0];
            let character = characterStart + line.indexOf(matched);
            edits.push(new vscode_1.TextEdit(new vscode_1.Range(new vscode_1.Position(index, character), new vscode_1.Position(index, character + matched.length)), "[%= "));
        }
        return edits;
    }
}
exports.EglFormatter = EglFormatter;
//# sourceMappingURL=format-provider.js.map