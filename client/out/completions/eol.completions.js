"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
exports.EolCompletions = [
    {
        label: ".isTypeOf()",
        kind: vscode_1.CompletionItemKind.Function,
        insertText: new vscode_1.SnippetString("isTypeOf(${1:element})"),
        detail: "isTypeOf(type: Type): Boolean",
        documentation: "Returns true if the object is of the given type and false otherwise"
    },
    {
        label: ".isKindOf()",
        kind: vscode_1.CompletionItemKind.Function,
        insertText: new vscode_1.SnippetString("isTypeOf(${1:element})"),
        detail: "<object>.isTypeOf(type: Type): Boolean",
        documentation: "Returns true if the object is of the given type or one of its subtypes and false otherwise"
    },
    {
        label: "import ",
        kind: vscode_1.CompletionItemKind.Keyword,
        insertText: new vscode_1.SnippetString("import '$1';"),
        detail: "import <module.eol>;",
        documentation: "An ETL module can import a number of other ETL modules"
    },
    {
        kind: vscode_1.CompletionItemKind.Function,
        label: ".err() ",
        insertText: new vscode_1.SnippetString("err('{1:[prefix : String]}');"),
        detail: "<object>.err([prefix : String]) : Any",
        documentation: "Prints a string representation of the object on which it is invoked to the error stream prefixed with the optional prefix string and returns the object on which it was invoked. In this way, the err operation can be used for debugging purposes in a non-invasive manner"
    },
    {
        label: ".format()",
        kind: vscode_1.CompletionItemKind.Function,
        insertText: new vscode_1.SnippetString("format('${1:[pattern : String]}');"),
        detail: "format([pattern : String]) : String",
        documentation: "Uses the provided pattern to form a String representation of the object on which the method is invoked. The pattern argument must conform to the format string syntax defined by Java"
    },
    {
        kind: vscode_1.CompletionItemKind.Function,
        label: ".println()",
        detail: "<object>.println([prefix: String]): Any",
        insertText: new vscode_1.SnippetString("println('${1:[prefix: String]}');"),
        documentation: "Has the same effects as the print operation but also produces a new line in the output stream."
    },
    {
        label: ".print()",
        kind: vscode_1.CompletionItemKind.Function,
        detail: "<object>.print([prefix: String]): Any",
        insertText: new vscode_1.SnippetString("print('${1:[prefix: String]}');"),
        documentation: "Prints  a  string  representation  of  the  object  on which it is invoked to the regular output stream,prefixed  with  the  optional prefix string  and  re-turns the object on which it was invoked.  In this way,  the print operation can be used for debugging purposes in a non-invasive manner"
    },
    {
        label: ".ifUndefined()",
        kind: vscode_1.CompletionItemKind.Function,
        insertText: new vscode_1.SnippetString("ifUnderfined($1);"),
        detail: "<object>.ifUndefined(alt: Any): Any",
        documentation: "If the object is undefined, it returns alt else it returns the object"
    },
    {
        label: ".hasProperty()",
        kind: vscode_1.CompletionItemKind.Function,
        insertText: new vscode_1.SnippetString("hasProperty('${1:name}');"),
        detail: "hasProperty(name: String): Boolean",
        documentation: "Returns true if the object has a property with the specified name or false otherwise"
    },
    {
        label: ".errln()",
        kind: vscode_1.CompletionItemKind.Function,
        detail: "errln([prefix : String]) : Any",
        insertText: new vscode_1.SnippetString("errln('${1:[prefix : String]}');"),
        documentation: "Has the same effects as the err operation but also produces a new line in the output stream."
    }
];
exports.eolCompletionProvider = vscode_1.languages.registerCompletionItemProvider("language-eol", {
    provideCompletionItems(document, position, token, context) {
        return exports.EolCompletions;
    }
});
//# sourceMappingURL=eol.completions.js.map