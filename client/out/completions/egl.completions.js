"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
exports.EglCompletions = [
    {
        label: 'static section',
        kind: vscode_1.CompletionItemKind.Snippet,
        insertText: new vscode_1.SnippetString('[%= ${1} %]'),
        detail: '[%= <section contents> %]',
        documentation: "Start of a static section."
    },
    {
        label: 'dynamic section',
        kind: vscode_1.CompletionItemKind.Snippet,
        insertText: new vscode_1.SnippetString('[% ${1} %]'),
        detail: '[%= <section contents> %]',
        documentation: "Start of a static section."
    }
];
exports.eglCompletionProvider = vscode_1.languages.registerCompletionItemProvider('language-egl', {
    provideCompletionItems(document, position, token, context) {
        return exports.EglCompletions;
    }
});
//# sourceMappingURL=egl.completions.js.map